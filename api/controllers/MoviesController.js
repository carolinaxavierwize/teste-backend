/**
 * FilmeController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const actionUtil = require('sails/lib/hooks/blueprints/actionUtil');

module.exports = {
    createMovies: async(req, res) => {
        const creatingData = actionUtil.parseValues(req);
        let user = req.session.user;
        if (user.administrator) {
            let textos = '';
            textos = textos.concat(creatingData.director, creatingData.title, creatingData.gender, creatingData.stars);
            creatingData.searchString = Util.RemoveDiacritics(textos);
            console.log('texts', creatingData.searchString);
            let created = await Movies.create(creatingData).fetch();
            if (created)
                return res.status(200).send(created);
            else
                return res.status(503);
        } else {
            return res.status(200).unauthorized({
                body: 'Somente administrador pode executar esta ação',
            })
        }
    },

    updateMovies: async(req, res) => {
        const creatingData = actionUtil.parseValues(req);
        let idValor = actionUtil.requirePk(req);
        let user = req.session.user;
        if (user.administrator) {
            let textos = '';
            textos = textos.concat(creatingData.director, creatingData.title, creatingData.gender, creatingData.stars);
            creatingData.searchString = Util.RemoveDiacritics(textos);
            let update = await Movies.update(idValor, creatingData).fetch();
            if (update)
                return res.status(200).send(update);
            else
                return res.status(503);
        } else {
            return res.status(200).unauthorized({
                body: 'Somente administrador pode executar esta ação',
            })
        }
    },

    inactive: async(req, res) => {
        let idValor = actionUtil.requirePk(req);
        let update = await Movies.update(idValor, { status: Movies.INATIVO }).fetch();
        if (update)
            return res.status(200).send(update);
        else
            return res.status(500).send('Não conseguimos concluir a ação');
    },

    search: async(req, res) => {
        let filterList = [];
        let searchString = req.query.searchString;
        if (searchString == null && categoryId == null) {
            return StatusCodes.Status400BadRequest, Constants.BadRequestErrorMessage;
        } else {

            if (searchString != null) {
                let listMovies = await Movies.find({ status: Movies.ATIVO });
                const lowerSearchFilters = searchString.toLowerCase();
                const searchList = [];
                for (let i = 0; i < listMovies.length; i++) {
                    if (listMovies[i].searchString) {
                        if ((listMovies[i].searchString.includes(lowerSearchFilters)) == true) {
                            searchList.push(listMovies[i]);
                        }
                        filterList = searchList;
                    }
                }
            }
        }
        return res.status(200).send(filterList);
    }
}